GatherButtonGcd.DefaultSettings = {
    -- Revert buttons back this many seconds before cooldown is over
    REVERT_TIME = 0.5,

    -- To combat chat spam, agressively try to revert buttons back after a
    -- a single press (may be unstable)
    AGRESSIVE_REVERT = false,

    -- We don't want to touch ground-targeted AOE abilities (rain of fire etc)
    -- because you won't be able to preemptively aim while the ability is on
    -- cooldown.
    --
    -- To get IDs you can use enemy addon:
    -- /script Enemy.GetAbilityIds("land mine") Enemy.GetAbilityIds("master rune of speed") Enemy.GetAbilityIds("master rune of adamant")
    IGNORED_ABILITIES = {
        -- This contains both abilities and their buff/debuffs that i'm too lazy to filter.
        [8177] = 1, -- rain of fire
        [4050] = 1, -- pit of shades
        [9485] = 1, -- pit of shades
        [8498] = 1, -- tzeentch's firestorm
        [8541] = 1, -- daemonic infestation
        [3997] = 1, -- dissolving mist
        [8494] = 1, -- dissolving mist
        [1537] = 1, -- napalm grenade
        [1524] = 1, -- land mine
        [4068] = 1, -- mistress of the marsh
        [9251] = 1, -- mistress of the marsh
        [1927] = 1, -- sticky feetz
        [3275] = 1, -- sticky feetz
        [8572] = 1, -- ritual of innervation
        [3836] = 1, -- ritual of superiority
        [8574] = 1, -- ritual of superiority
        [3618] = 1, -- ritual of lunacy
        [8577] = 1, -- ritual of lunacy
        [1612] = 1, -- master rune of fury
        [3643] = 1, -- master rune of fury
        [1615] = 1, -- master rune of speed
        [3642] = 1, -- master rune of speed
        [1618] = 1, -- master rune of adamant
        [3641] = 1 -- master rune of adamant (?)
    }
}
