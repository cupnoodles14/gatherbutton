<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="GatherButtonGcd" version="1.0.4" date="2020-07-16">
		<VersionSettings gameVersion="1.4.8" windowsVersion="1.0" savedVariablesVersion="1.0" />

		<Author name="cupnoodles" email="cupn8dles@gmail.com" />
		<Description text="Pressing hotbar buttons on cooldown will gather plants." />

		<Dependencies>
			<Dependency name="GatherButton"/>
			<Dependency name="EA_ActionBars"/>
		</Dependencies>

		<Files>
			<File name="gatherbuttongcd.lua" />
			<File name="config.lua" />
		</Files>

		<OnInitialize>
			<CallFunction name="GatherButtonGcd.Initialize" />
		</OnInitialize>

		<OnShutdown>
			<CallFunction name="GatherButtonGcd.Unhook" />
		</OnShutdown>

		<WARInfo>
			<Categories>
				<Category name="CRAFTING" />
				<Category name="ACTION_BARS" />
			</Categories>
			<Careers>
				<Career name="BLACKGUARD" />
				<Career name="WITCH_ELF" />
				<Career name="DISCIPLE" />
				<Career name="SORCERER" />
				<Career name="IRON_BREAKER" />
				<Career name="SLAYER" />
				<Career name="RUNE_PRIEST" />
				<Career name="ENGINEER" />
				<Career name="BLACK_ORC" />
				<Career name="CHOPPA" />
				<Career name="SHAMAN" />
				<Career name="SQUIG_HERDER" />
				<Career name="WITCH_HUNTER" />
				<Career name="KNIGHT" />
				<Career name="BRIGHT_WIZARD" />
				<Career name="WARRIOR_PRIEST" />
				<Career name="CHOSEN" />
				<Career name="MARAUDER" />
				<Career name="ZEALOT" />
				<Career name="MAGUS" />
				<Career name="SWORDMASTER" />
				<Career name="SHADOW_WARRIOR" />
				<Career name="WHITE_LION" />
				<Career name="ARCHMAGE" />
			</Careers>
		</WARInfo>
	</UiMod>
</ModuleFile>
