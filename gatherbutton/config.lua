GatherButton.DefaultSettings = {
    -- If enabled, settings won't be overwritten with default values on initialization
    -- and it will be possible to configure addon from ingame. For example:
    --
    --      /script GatherButton.Settings.PERSISTENT_SETTINGS = true; GatherButton.Settings.GLOW_TYPE_ORDER = false
    --
    PERSISTENT_SETTINGS = false,

    -- Use miracle grow to repeat if available (supports autoadding nutrients/water/soil)
    MG_REPEAT = true,

    -- If MG_REPEAT is false (or MGR is not present), automatically refining would
    -- attempt to keep N seeds. If MGR is enabled, MGR uses its own config.
    SEEDS_TO_KEEP = 6,

    -- Workarounds so that we don't do 40+ requests to refine/repeat at once.
    -- (For one repeat requests we can issue several refine requests so we have two counters).
    MAX_PENDING_REFINE_REQUESTS = 6,
    MAX_PENDING_REPEAT_REQUESTS = 1,

    -- Choose notification type (see main .lua file for possible NOTIFICATION_ values)
    NOTIFICATION_TYPE = GatherButton.NOTIFICATION_NONE
}
