GatherButton.SlashCommands = {
    ["toggle"] = {
        description = "toggle autorepeat",
        handler = GatherButton.Toggle
    },

    ["reset"] = {
        description = "reset persistent settings",
        handler = function()
            GatherButton.Settings.PERSISTENT_SETTINGS = false
            GatherButton.ChatPrint(
                "Persistent settings disabled. Reload UI or logout to finalize.")
        end
    },

    ["glow-type"] = {
        description = "set glow type",
        function(args)
            if not GatherButton.Settings.PERSISTENT_SETTINGS then
                GatherButton.Settings.PERSISTENT_SETTINGS = true
                GatherButton.ChatPrint("Persistent settings enabled.")
            end

            local BUTTON = GatherButtonMacro.Button
            if #args == 0 then
                GatherButtonMacro.SetGlowType(BUTTON, nil)
                GatherButton.ChatPrint("Glow type: auto")
                return
            end

            local c = string.sub(args[1], 1, 1)
            if c == "o" then
                GatherButtonMacro.SetGlowType(BUTTON, true)
                GatherButton.ChatPrint("Glow type: order")
            elseif c == "d" then
                GatherButtonMacro.SetGlowType(BUTTON, false)
                GatherButton.ChatPrint("Glow type: destro")
            else
                GatherButtonMacro.SetGlowType(BUTTON, nil)
                GatherButton.ChatPrint("Glow type: auto")
            end
        end
    }
}

function GatherButton.SetSlashSubcommandHandler(cmd, handler, description)
    GatherButton.SlashCommands[cmd] = {
        description = description,
        handler = handler
    }
end

function GatherButton.Help()
    local commands = ""
    for k, v in pairs(GatherButton.SlashCommands) do
        commands = commands .. "    " .. k .. " - " .. v.description .. "\n"
        -- commands = commands .. string.format("% 2s% 8s", "abc", "def")
    end

    GatherButton.ChatPrint("Possible commands:\n" .. commands)
    return
end

function GatherButton.Slash(msg)
    msg = msg:lower()
    local args = StringSplit(msg)
    local cmd = table.remove(args, 1)

    if not GatherButton.SlashCommands[cmd] then
        GatherButton.ChatPrint("Unknown command: '" .. msg .. "'")
        GatherButton.Help()
        return
    end

    GatherButton.SlashCommands[cmd].handler(args)
end
