-- Macro functions are based on Enemy ones
-- Enemy is licensed under MIT https://web.archive.org/web/20110916141417/http://war.curse.com/downloads/war-addons/details/enemy.aspx
--
GatherButtonMacro = {}

GatherButtonMacro.Button = nil
GatherButtonMacro.MacroId = 0

function GatherButtonMacro.Initialize()
    if not GatherButtonMacro.Settings or
        not GatherButton.Settings.PERSISTENT_SETTINGS then
        GatherButtonMacro.Settings = GatherButtonMacro.DefaultSettings
    end

    GatherButtonMacro.MacroId = GatherButtonMacro.CreateMacro(
                                    GatherButtonMacro.Settings.GatherMacro.Name,
                                    GatherButtonMacro.Settings.GatherMacro.Text,
                                    GatherButtonMacro.Settings.GatherMacro.Icon)
    if (not GatherButtonMacro.MacroId) then
        GatherButton.ChatPrint(
            "GatherButton failed to initialize (cannot find or create the macro). You may not have an empty macro slot (text and name fields should be empty).")
        return
    end

    local m = GatherButtonMacro.CreateMacro(
                  GatherButtonMacro.Settings.ToggleMacro.Name,
                  GatherButtonMacro.Settings.ToggleMacro.Text,
                  GatherButtonMacro.Settings.ToggleMacro.Icon)
    if (not m) then
        GatherButton.ChatPrint(
            "GatherButton couldn't make a toggle button. Use /gbtoggle or /script GatherButton.Toggle() to toggle autorepeating.")
    end

    ActionButton.SetActionData = GatherButtonMacro.HookSetActionData(
                                     ActionButton.SetActionData)
end

function GatherButtonMacro.GetMacroId(text)
    local macros = DataUtils.GetMacros()
    for slot = 1, EA_Window_Macro.NUM_MACROS do
        if macros[slot].text == text then
            return slot
        end
    end

    return nil
end

function GatherButtonMacro.SetMacro(slot, name, text, iconId)
    SetMacroData(name, text, iconId, slot)
    EA_Window_Macro.UpdateDetails(slot)
end

function GatherButtonMacro.CreateMacro(name, text, iconId)
    -- try to find an existing macro
    local macroSlot = GatherButtonMacro.GetMacroId(text)
    if (macroSlot) then
        GatherButtonMacro.SetMacro(macroSlot, name, text, iconId) -- reset icon and name just in case
        return macroSlot
    end

    GatherButton.ChatPrint("No macro found. Creating a new macro.")

    -- try to make a new macro
    local macros = DataUtils.GetMacros()
    for slot = 1, EA_Window_Macro.NUM_MACROS do
        if (macros[slot].text == L"" and macros[slot].name == L"") then
            GatherButtonMacro.SetMacro(slot, name, text, iconId)
            GatherButton.ChatPrint(
                "New macro (slot " .. tostring(slot) .. "): " ..
                    GatherButton.MakeIconString(iconId) .. " " .. tostring(name))
            return slot
        end
    end

    GatherButton.ChatPrint("Failed to find an empty macro slot.")

    return nil
end

function GatherButtonMacro.HookSetActionData(fn)
    return function(self, actionType, actionId)
        fn(self, actionType, actionId)

        if (actionType ~= GameData.PlayerActions.DO_MACRO) or
            (actionId ~= GatherButtonMacro.MacroId) then
            return
        end
        local actionName = self:GetName() .. "Action"
        WindowSetGameActionData(actionName,
                                GameData.PlayerActions.PERFORM_CRAFTING,
                                GameData.TradeSkills.CULTIVATION, L"")

        GatherButtonMacro.Button = self
        GatherButton.CraftUpdate()

        if GatherButtonMacro.Settings.GLOW_TYPE_ORDER ~= nil then
            GatherButtonMacro.SetGlowType(GatherButtonMacro.Button,
                                          GatherButtonMacro.Settings
                                              .GLOW_TYPE_ORDER)
        end
    end
end

