local BASE_ICON = 0
local COOLDOWN_TIMER = 3
local FLASH_ANIM = 4
local GLOW_ANIM = 6
local STACK_COUNT_TEXT = 7

function GatherButtonMacro.SetTint(button, tint)
    if not button then
        return
    end

    button.m_Windows[BASE_ICON]:SetTintColor(tint.r, tint.g, tint.b)
end

function GatherButtonMacro.FlashButton(button)
    if not button then
        return
    end

    button.m_Windows[FLASH_ANIM]:StartAnimation(0, false, true, 0)
end

function GatherButtonMacro.ShowStackText(button, show)
    if not button then
        return
    end

    local stackText = button.m_Windows[STACK_COUNT_TEXT]
    stackText:Show(show)
end

function GatherButtonMacro.SetStackText(button, text)
    if not button then
        return
    end

    local stackText = button.m_Windows[STACK_COUNT_TEXT]
    stackText:Show(true)
    stackText:SetText(text)
end

function GatherButtonMacro.SetCooldownText(button, text)
    if not button then
        return
    end

    -- local timerFrame = button.m_Windows[COOLDOWN_TIMER]
    button.m_Cooldown = text
    button.m_MaxCooldown = 180
end

function GatherButtonMacro.SetGlowType(button, isArchmageType)
    if (isArchmageType) then
        button.m_GlowBase = "anim_fury_"
        return
    end

    button.m_GlowBase = "anim_waaagh_"
end

function GatherButtonMacro.SetGlow(button, glowLevel)
    if not button then
        return
    end

    local glowFrame = button.m_Windows[GLOW_ANIM]

    if glowLevel == 0 then
        glowFrame:StopAnimation(true)
        glowFrame:Show(false)
        return
    end

    glowFrame:Show(true)
    glowFrame:StopAnimation(true)
    glowFrame:SetAnimationTexture(button.m_GlowBase .. glowLevel)
    glowFrame:StartAnimation(0, true, false, 0)
    button.m_GlowLevel = glowLevel

    local t = GatherButtonMacro.Settings.GLOW_TINT
    WindowSetTintColor(glowFrame:GetName(), t.r, t.g, t.b)
end

function GatherButtonMacro.ResetButton()
    GatherButtonMacro.ResetButtonAction(GatherButtonMacro.MacroId)
end

function GatherButtonMacro.ResetButtonAction(id)
    -- find a 'do macro' button on a hotbar with our macroId and set it to perform crafting
    for btnNum = 1, 60 do
        local actionType, actionId, m_IsEnabled, m_IsTargetValid, m_IsBlocked =
            GetHotbarData(btnNum)
        if (actionType == GameData.PlayerActions.DO_MACRO) and (actionId == id) then
            -- initialize it
            local barObject, buttonId =
                ActionBars:BarAndButtonIdFromSlot(btnNum)
            if (barObject ~= nil) then
                barObject:SetButtonData(buttonId, actionType, actionId) -- this calls GatherButton.SetActionData()
                break
            end
        end
    end
end
