-- Repeat plot functions (code mostly taken from MiracleGrow)
-- MGRemix is in public domain:
-- https://web.archive.org/web/20110807033827/http://war.curse.com/downloads/war-addons/details/mgremix.aspx
local REFINE_REQUESTS = GatherButton.RefineRequests
local REPEAT_REQUESTS_PENDING = GatherButton.RepeatRequests

-- DecayPendingRequests decrements pending request counts.
-- Like, call it once a second or so.
function GatherButton.DecayPendingRequests()
    for k, v in pairs(REFINE_REQUESTS) do
        if v > 0 then
            REFINE_REQUESTS[k] = v - 1
        end
    end

    for i, v in ipairs(REPEAT_REQUESTS_PENDING) do
        if v > 0 then
            REPEAT_REQUESTS_PENDING[i] = v - 1
        end
    end
end

function GatherButton.RepeatPlot(plotNum)
    if REPEAT_REQUESTS_PENDING[plotNum] == nil then
        REPEAT_REQUESTS_PENDING[plotNum] = 0
    end
    if REPEAT_REQUESTS_PENDING[plotNum] >=
        GatherButton.Settings.MAX_PENDING_REPEAT_REQUESTS then
        return
    end

    local seedId = GatherButton.LastGrown[plotNum]
    if not seedId then
        return
    end

    GatherButton.AttemptRefinePlants(seedId)

    local slot, item = GatherButton.GetSlotByItemId(seedId)
    if slot <= 0 then
        return
    end

    AddCraftingItem(GameData.ItemLocs.INVENTORY, plotNum, slot,
                    EA_Window_Backpack.TYPE_CRAFTING) -- NOTE: instead of GameData.ItemLocs.INVENTORY there was a `3` in MGR source
    -- i'm trying to get rid of this magic number but i may be wrong
    REPEAT_REQUESTS_PENDING[plotNum] = REPEAT_REQUESTS_PENDING[plotNum] + 1
end

function GatherButton.AttemptRefinePlants(seedId)
    local seedCount = GatherButton.GetItemCount(seedId)
    if seedCount >= GatherButton.Settings.SEEDS_TO_KEEP then
        return
    end
    GatherButton.RefinePlant(seedId,
                             GatherButton.Settings.SEEDS_TO_KEEP - seedCount)
end

function GatherButton.RefinePlant(seedId, count)
    local aSeedInfo = CraftValueTip.SeedList[seedId]
    if (not aSeedInfo) or (aSeedInfo[4] == 0) then -- or (aSeedInfo[1] ~= "std")
        d("Don't know how to make more " .. seedId)
        return
    end

    local plantId = aSeedInfo[4]
    GatherButton.RefineItem(plantId, count)
end

function GatherButton.RefineItem(uniqueID, nCount)
    local nSlot, vItem
    local nLeft = nCount
    if nLeft <= 0 then
        return
    end
    nSlot, vItem = GatherButton.GetSlotByItemId(uniqueID)

    if nSlot == 0 or not vItem or not vItem.isRefinable then
        return
    end
    local nGlobalLoc = GatherButton.GetCursorForBackpack(
                           EA_Window_Backpack.TYPE_CRAFTING) -- 30

    if REFINE_REQUESTS[uniqueID] == nil then
        REFINE_REQUESTS[uniqueID] = 0
    end
    for i = 1, math.min(nLeft, vItem.stackCount) do
        -- d(" use "..nGlobalLoc.." "..nSlot)
        if REFINE_REQUESTS[uniqueID] >
            GatherButton.Settings.MAX_PENDING_REFINE_REQUESTS then
            return
        end

        REFINE_REQUESTS[uniqueID] = REFINE_REQUESTS[uniqueID] + 1
        SendUseItem(nGlobalLoc, nSlot, 0, 0, 0) -- /script SendUseItem(30, 17, 0, 0, 0)
    end
end

function GatherButton.GetItemCount(uniqueID)
    local iCount = 0
    local vBagItems = DataUtils.GetCraftingItems()
    for k, v in pairs(vBagItems) do
        if (v.uniqueID == uniqueID) then
            iCount = iCount + v.stackCount
        end
    end
    return iCount
end

function GatherButton.GetSlotByItemId(uniqueID)
    local iSmall = 10000
    local slot = 0
    local item
    local vBagItems = DataUtils.GetCraftingItems()
    for k, v in pairs(vBagItems) do
        if (v.uniqueID == uniqueID) and (v.stackCount < iSmall) then
            slot = k
            iSmall = v.stackCount
            item = v
        end
    end
    return slot, item
end

function GatherButton.GetCursorForBackpack(nBackpackType)
    if EA_Window_Backpack.GetCursorForBackpack then
        return EA_Window_Backpack.GetCursorForBackpack(nBackpackType)
    end

    return GameData.ItemLocs.INVENTORY -- 3
end

