<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <UiMod name="GatherButton" version="1.0.4" date="2020-07-16">
        <VersionSettings gameVersion="1.4.8" windowsVersion="1.0" savedVariablesVersion="1.0"/>

        <Author name="cupnoodles" email="cupn8dles@gmail.com" />
        <Description text="Adds a <icon699> button to gather grown plants. Seeds should be automatically planted again. 
You can find the button in macro window along with a macro to temporarily toggle autorepeat functionality so you could swap seeds. Alternatively, you can use /gbtoggle command.
A quick way to plant all 4 seeds is by opening cultivation window and right-clicking the seeds.
If Miracle Grow Remix is enabled, soil/water/nutrients will also be readded automatically.
Requires: Crafting Info Tooltip (aka CraftValueTip), LibSlash"/>

        <Dependencies>
            <Dependency name="EA_ActionBars"/>
            <Dependency name="Crafting Info Tooltip"/>
            <Dependency name="LibSlash"/>
        </Dependencies>

        <Files>
            <File name="gatherbutton.lua" />
            <File name="util.lua" />
            <File name="slash.lua" />
            <File name="repeat.lua" />
            <File name="config.lua" />
            <File name="macro/macro.lua" />
            <File name="macro/button.lua" />
            <File name="macro/config.lua" />
        </Files>

        <SavedVariables>
            <SavedVariable name="GatherButton.Settings" />
            <SavedVariable name="GatherButtonMacro.Settings" />
        </SavedVariables>

        <OnInitialize>
            <CallFunction name="GatherButton.Initialize" />
            <!-- <CallFunction name="GatherButtonMacro.Initialize" /> -->
        </OnInitialize>

        <OnUpdate/>

        <OnShutdown/>

        <WARInfo>
            <Categories>
                <Category name="CRAFTING" />
            </Categories>
            <Careers>
                <Career name="BLACKGUARD" />
                <Career name="WITCH_ELF" />
                <Career name="DISCIPLE" />
                <Career name="SORCERER" />
                <Career name="IRON_BREAKER" />
                <Career name="SLAYER" />
                <Career name="RUNE_PRIEST" />
                <Career name="ENGINEER" />
                <Career name="BLACK_ORC" />
                <Career name="CHOPPA" />
                <Career name="SHAMAN" />
                <Career name="SQUIG_HERDER" />
                <Career name="WITCH_HUNTER" />
                <Career name="KNIGHT" />
                <Career name="BRIGHT_WIZARD" />
                <Career name="WARRIOR_PRIEST" />
                <Career name="CHOSEN" />
                <Career name="MARAUDER" />
                <Career name="ZEALOT" />
                <Career name="MAGUS" />
                <Career name="SWORDMASTER" />
                <Career name="SHADOW_WARRIOR" />
                <Career name="WHITE_LION" />
                <Career name="ARCHMAGE" />
            </Careers>
        </WARInfo>

    </UiMod>
</ModuleFile>
