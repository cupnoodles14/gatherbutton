--[[ --------------------------------------------------------------------------
MIT License

Copyright (c) 2019 cupnoodles

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
--]] --------------------------------------------------------------------------
GatherButton = {}

GatherButton.NOTIFICATION_NONE = 0
GatherButton.NOTIFICATION_CHAT = 1 -- show message in chat
GatherButton.NOTIFICATION_BUBBLE = 2 -- show bubble message (very intrusive)
GatherButton.NOTIFICATION_POINT_GAIN = 3 -- show "point gain" (i.e. something like influence scrolling text.
-- except it either looks bad or it'll have +INF/+EXP/+RR suffix if you change the related code)

-- script global variables
GatherButton.RefineRequests = {}
GatherButton.RepeatRequests = {[1] = 0, [2] = 0, [3] = 0, [4] = 0}
GatherButton.PlotData = {[1] = nil, [2] = nil, [3] = nil, [4] = nil}
GatherButton.PlotReady = {[1] = true, [2] = true, [3] = true, [4] = true}
GatherButton.LastGrown = {}
local TIME = 0
local AUTOREPEAT_ENABLED = true
local READY_NOTIFIED = false

function GatherButton.Initialize()
    if not GatherButton.Settings or
        not GatherButton.Settings.PERSISTENT_SETTINGS then
        GatherButton.Settings = GatherButton.DefaultSettings
    end

    RegisterEventHandler(SystemData.Events.PLAYER_CULTIVATION_UPDATED,
                         "GatherButton.UpdatePlotFromServer")
    RegisterEventHandler(SystemData.Events.PLAYER_CRAFTING_SLOT_UPDATED,
                         "GatherButton.CraftUpdate")
    RegisterEventHandler(SystemData.Events.UPDATE_PROCESSED,
                         "GatherButton.OnUpdate")

    LibSlash.RegisterSlashCmd("gbtoggle", GatherButton.Toggle)
    LibSlash.RegisterSlashCmd("gabu", GatherButton.Slash)

    GatherButtonMacro.Initialize()

    GatherButton.ChatPrint("GatherButton initialized.")
end

function GatherButton.UpdatePlotFromServer()
    local plotNum = GameData.Player.Cultivation.UpdatedIndex
    GatherButton.UpdatePlot(plotNum)
end

function GatherButton.UpdatePlot(plotNum)
    local plotInfo = GetCultivationInfo(plotNum)
    GatherButton.PlotData[plotNum] = plotInfo
    GatherButton.PlotReady[plotNum] = (plotInfo.StageNum == 4)

    GatherButton.CraftUpdate()
end

function GatherButton.OnUpdate(timeElapsed)
    -- throttling (check plots once a second)
    TIME = TIME + timeElapsed
    if TIME <= 1 then
        return
    end
    TIME = TIME - 1

    -- update cooldown text on the button
    local timeleft = 300 -- bigger than any possible plant time. hacky but whatever
    local plotinfo
    for plotNum = 1, CultivationWindow.NUM_OF_PLOTS do
        plotinfo = GatherButton.PlotData[plotNum]
        if plotinfo then
            plotinfo.TotalTimer = plotinfo.TotalTimer - 1
            if plotinfo.TotalTimer < 0 then
                plotinfo.TotalTimer = 0
            end

            if plotinfo.TotalTimer < timeleft then
                timeleft = plotinfo.TotalTimer
            end
        end
    end

    GatherButtonMacro.SetCooldownText(GatherButtonMacro.Button, timeleft + 1) -- +1 second just because sometimes it's imprecise
    GatherButtonMacro.ShowStackText(GatherButtonMacro.Button, true) -- for some reason, stack text sometimes disappears

    -- manage pending refine requests (decrement one each second)
    GatherButton.DecayPendingRequests()
end

function GatherButton.SelectNextReady()
    local current = GameData.Player.Cultivation.CurrentPlot
    for i = current, current + CultivationWindow.NUM_OF_PLOTS do
        local plotNum = (i % CultivationWindow.NUM_OF_PLOTS) + 1
        if GatherButton.PlotReady[plotNum] and plotNum ~= current then
            GameData.Player.Cultivation.CurrentPlot = plotNum
            return true
        end
    end
    return false
end

function GatherButton.Toggle()
    AUTOREPEAT_ENABLED = not AUTOREPEAT_ENABLED
    if AUTOREPEAT_ENABLED then
        GatherButton.ChatPrint("GatherButton autorepeat enabled.")
        GatherButtonMacro.SetTint(GatherButtonMacro.Button,
                                  GatherButtonMacro.Settings.TINT_ENABLED)
    else
        GatherButton.ChatPrint(
            "GatherButton autorepeat disabled. You can now change the seeds.")
        GatherButtonMacro.SetTint(GatherButtonMacro.Button,
                                  GatherButtonMacro.Settings.TINT_DISABLED)
    end
end

-- Usually, we enter this function when some plot has finished growing or
-- after we had gathered it.
--
-- It can be called _at least_ 8 times in a quick succession - it has to
-- select a plot for the user to gather, then detect that the plot is empty
-- and plant a new plant. Repeat 4 times as there are 4 plots.
-- Will be called even more often if there are hooks to inventory update events,
-- etc.
-- So, it shouldn't perform some massive calculations every time.
function GatherButton.CraftUpdate()
    if not GatherButtonMacro.Button then
        -- on game start, gatherbutton is a standard macro GatherButtonMacro.Button.
        -- so we need to manually find it and change 'do macro' action to 'do crafting'
        -- calling it in Initialize() may be too early so we call it here
        GatherButtonMacro.ResetButton()
    end

    local growingcount = 0
    local growncount = 0
    local newtext = L""
    GameData.Player.Cultivation.CurrentPlot = 0

    for plotNum = 1, CultivationWindow.NUM_OF_PLOTS do
        local plotinfo = GatherButton.PlotData[plotNum]
        if plotinfo then
            -- update macro name with plot icon so it can be checked by hovering over the button
            newtext = newtext ..
                          towstring(GatherButton.MakeIconString(
                                        plotinfo.Seed.iconNum)) .. L" "

            if plotinfo.StageNum == 4 then
                -- plot has finished growing
                -- remember the seed, switch to this plot for gathering
                growncount = growncount + 1
                GatherButton.LastGrown[plotNum] = plotinfo.Seed.uniqueID
                -- select plot as current so it could be gathered
                GameData.Player.Cultivation.CurrentPlot = plotNum
            elseif plotinfo.StageNum == 0 then
                -- switching CurrentPlot also allows to plant multiple seeds without
                -- opening each plot individually:
                GameData.Player.Cultivation.CurrentPlot = plotNum

                if AUTOREPEAT_ENABLED then
                    -- plot is empty, repeat
                    if MiracleGrow2 and GatherButton.Settings.MG_REPEAT then
                        MiracleGrow2.Repeat(plotNum)
                    else
                        GatherButton.RepeatPlot(plotNum)
                    end
                end
            else
                -- plot is growing, just count it
                growingcount = growingcount + 1
            end
        end
    end

    --
    -- visual feedback
    --

    GatherButtonMacro.SetMacro(GatherButtonMacro.MacroId, newtext,
                               GatherButtonMacro.Settings.GatherMacro.Text,
                               GatherButtonMacro.Settings.GatherMacro.Icon)
    GatherButtonMacro.SetGlow(GatherButtonMacro.Button, growncount)

    local totalcount = growncount + growingcount
    GatherButtonMacro.SetStackText(GatherButtonMacro.Button, towstring(
                                       growncount) .. L"/" ..
                                       towstring(totalcount))

    if not READY_NOTIFIED and totalcount ~= 0 and growingcount == 0 then
        READY_NOTIFIED = true
        GatherButtonMacro.FlashButton(GatherButtonMacro.Button)
        GatherButton.OnAllPlantsFinished(growncount)
    elseif READY_NOTIFIED and growingcount ~= 0 then
        READY_NOTIFIED = false
    end
end

function GatherButton.OnAllPlantsFinished(growncount)
    if GatherButton.Settings.NOTIFICATION_TYPE ~= GatherButton.NOTIFICATION_NONE then
        GatherButton.Notify(GatherButton.Settings.NOTIFICATION_TYPE, growncount)
    end
end

function GatherButton.Notify(notify_type, growncount)
    if notify_type == GatherButton.NOTIFICATION_BUBBLE then
        ChatManager.AddChatText(GameData.Player.worldObjNum,
                                L"I probably should gather the plants...")
    elseif notify_type == GatherButton.NOTIFICATION_CHAT then
        GatherButton.ChatPrint("There are plants ready to be collected")
    elseif notify_type == GatherButton.NOTIFICATION_POINT_GAIN then
        EA_System_EventText.AddPointGain(GameData.Player.worldObjNum, {
            event = 2,
            amount = L"Grown plant" .. towstring(tostring(growncount)),
            type = 0
        })
    end
end

